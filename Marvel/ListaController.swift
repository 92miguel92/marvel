//
//  ListaController.swift
//  Marvel
//
//  Created by Miguel Ángel Jareño Escudero on 31/10/16.
//  Copyright © 2016 Miguel Ángel Jareño Escudero. All rights reserved.
//

import UIKit
import Marvelous

class ListaController: UIViewController, UISearchBarDelegate, UITableViewDataSource{

    //var controller = ViewController()
    var personajes = [String]()

    /*override func perform(){
        let src = ListaController
        let dst = DetalleViewController
        src.navigationController?.
    }*/
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.personajes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "unaCelda", for: indexPath)
        let cell = UITableViewCell(style: .default, reuseIdentifier: "")
        //cell.textLabel?.text = self.nombres[indexPath.row]
        cell.textLabel?.text = self.personajes[indexPath.row]
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let detail = segue as! DetalleViewController
        var indexPath = tableView.indexPathForSelectedRow
        detail.nombre = personajes[(indexPath?.row)!]
    }
    
    /*func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let id = indexPath.row
        self.performSegue(withIdentifier: "DetailScreenSegue", sender: self)
    }*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        spinner.startAnimating()
        let controller = ViewController()
        print("Buscado: \(searchBar.text!)")
        searchBar.resignFirstResponder()
        self.personajes = controller.seePersonajes(subStrPersonajes: searchBar.text!)
        OperationQueue.main.addOperation {
            self.tableView.reloadData()
        }
        spinner.stopAnimating()
        /*self.nombres.insert(conTexto, at:enFila)
        let indexPath = IndexPath(row:controller.personajes.count, section:0)
        tableView.insertRows(at: [indexPath], with: UITableViewRowAnimation.fade)*/
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
