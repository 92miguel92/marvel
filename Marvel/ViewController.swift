//
//  ViewController.swift
//  Marvel
//
//  Created by Miguel Ángel Jareño Escudero on 31/10/16.
//  Copyright © 2016 Miguel Ángel Jareño Escudero. All rights reserved.
//

import UIKit
import Marvelous

class ViewController: UIViewController {
    
    //var lController = ListaController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    func seePersonajes(subStrPersonajes: String)-> [String]{
        var lPersonajes = [String]()
        let marvelAPI = RCMarvelAPI()
        marvelAPI.publicKey = "ab6d41f0c5fa82e8268c50fc6ed31169"
        marvelAPI.privateKey = "4058f749680e883aef4718b815728ea4d6073809"
        let filtro = RCCharacterFilter()
        filtro.nameStartsWith = subStrPersonajes
        marvelAPI.characters(by: filtro){
            resultados, info, error in
            if let personajes = resultados as! [RCCharacterObject]? {
                for personaje in personajes {
                    print(personaje.name)
                    lPersonajes.append(personaje.name)
                }
                print("Hay \(personajes.count) personajes")
            }
        }
        sleep(3)
        return lPersonajes
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

