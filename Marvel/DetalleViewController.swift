//
//  DetalleViewController.swift
//  Marvel
//
//  Created by Miguel Ángel Jareño Escudero on 31/10/16.
//  Copyright © 2016 Miguel Ángel Jareño Escudero. All rights reserved.
//

import UIKit
import Marvelous

class DetalleViewController: UIViewController {

    //var personaje = RCCharacterObject()
    @IBOutlet weak var tittle: UILabel!
    @IBOutlet weak var descripcion: UITextView!
    @IBOutlet weak var image: UIImageView!
    var nombre: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //var lPersonajes = [String]()
        let marvelAPI = RCMarvelAPI()
        marvelAPI.publicKey = "ab6d41f0c5fa82e8268c50fc6ed31169"
        marvelAPI.privateKey = "4058f749680e883aef4718b815728ea4d6073809"
        let filtro = RCCharacterFilter()
        filtro.nameStartsWith = self.nombre
        marvelAPI.characters(by: filtro){
            resultados, info, error in
            if let personajes = resultados as! [RCCharacterObject]? {
                for personaje in personajes {
                    print(personaje.name)
                    //image.image = UIImage(personaje.thumbnail)
                    self.tittle.text = personaje.name
                    self.descripcion.text = personaje.bio
                }
                print("Hay \(personajes.count) personajes")
            }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
